# Event+


## Overview

TK Basdat, semoga kelar, aamiin.


## Installation

- Clone this repository (with HTTPS preferred)
    ```bash
    $ git clone https://gitlab.com/AldiNFitrah/event-plus.git
    ```

- Activate virtualenv, or create one if none has been created
    ```bash
    $ virtualenv env
    ```

- Create .env file from .env.example  
    ```bash
    $ cp .env.example .env
    ```
    - Then fill in required fields e.g. Secret Key and DB configs

- Install required packages
    ```bash
    $ pip install -r requirements.txt
    ```

- Run the server in your local (`localhost:8000`)  
    ```bash
    $ python manage.py runserver
    ```


## Development

- Create a new branch from `master` with:  
    ```bash
    $ git checkout -b <your_name/scope>
    ```
    - example:
        - `aldi/e-wallet`

- Do your changes, then push to remote repository to be merged  
    ```bash
    $ git add .
    $ git commit -m "<description>"
    $ git push origin <your_branch>
    ```

- Submit merge request on the remote repository, wait for approvals, then merge if approved.

- After merge:
    ```bash
    $ git checkout master (or the target branch on the merge request)
    $ git pull origin master
    ```
- **Repeat**
