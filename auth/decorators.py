from django.contrib.auth.decorators import user_passes_test
from django.core.exceptions import PermissionDenied
from django.shortcuts import redirect, reverse


def login_required(*outer_args, **outer_kwargs):
    def _view_wrapper(func):
        def _args_wrapper(request, *args, **kwargs):
            if request.session.get("email") is None:
                return redirect(reverse("auth:login") + f"?next={request.path}")

            return func(request, *args, **kwargs)

        return _args_wrapper

    return _view_wrapper


def login_forbidden(*outer_args, **outer_kwargs):
    def _view_wrapper(func):
        def _args_wrapper(request, *args, **kwargs):
            if request.session.get("email") is not None:
                return redirect("/")

            return func(request, *args, **kwargs)

        return _args_wrapper

    return _view_wrapper


def role_required(required_role):
    def _view_wrapper(func):
        def _args_wrapper(request, *args, **kwargs):
            if required_role not in request.session.get("role", "user"):
                raise PermissionDenied()

            return func(request, *args, **kwargs)

        return _args_wrapper

    return _view_wrapper
