from django import forms
from django.db import connection

from auth.utils import hashed


class UserLoginForm(forms.Form):
    email = forms.EmailField()
    password = forms.CharField(widget=forms.PasswordInput)


class UserRegisterForm(forms.Form):
    email = forms.EmailField(max_length=50)
    password = forms.CharField(
        min_length=6,
        max_length=50,
        widget=forms.PasswordInput,
    )

    def __init__(self, *args, **kwargs):
        self.for_update = kwargs.pop("for_update", False)

        super().__init__(*args, **kwargs)

        if self.for_update:
            self.fields.pop("password")

    def clean_password(self):
        password = self.cleaned_data["password"]
        errors = []

        if not any([char.isupper() for char in password]):
            errors.append("Password must contains a capital (uppercase) letter")

        if not any([char.isdigit() for char in password]):
            errors.append("Password must contains a number")

        if all([char.isalnum() for char in password]):
            errors.append("Password must contains a symbol")

        if errors:
            raise forms.ValidationError(errors)

        # Don't store actual password in db
        return hashed(password)

    def save(self):
        data = ["email", "password"]

        if self.for_update:
            # Used as argument in the SQL query, using the key in data
            data = ["email"]
            set_args = ", ".join([d + "=%s" for d in data])
            where_args = " AND ".join([d + "=%s" for d in data])
            with connection.cursor() as cursor:
                cursor.execute(
                    f"UPDATE pengguna SET {set_args} WHERE {where_args}",
                    [self.cleaned_data[d] for d in data]
                    + [self.initial[d] for d in data],
                )

        else:
            with connection.cursor() as cursor:
                cursor.execute(
                    "INSERT INTO pengguna VALUES(%s, %s)",
                    [self.cleaned_data[d] for d in data],
                )


class VisitorRegisterForm(UserRegisterForm):
    nama_depan = forms.CharField(max_length=20)
    nama_belakang = forms.CharField(max_length=20)
    alamat = forms.CharField(max_length=100)

    def save(self):
        super().save()

        data = ["email", "nama_depan", "nama_belakang", "alamat"]

        if self.for_update:
            # Used as argument in the SQL query, using the key in data
            set_args = ", ".join([d + "=%s" for d in data])
            where_args = " AND ".join([d + "=%s" for d in data])
            with connection.cursor() as cursor:
                cursor.execute(
                    f"UPDATE pengunjung SET {set_args} WHERE {where_args}",
                    [self.cleaned_data[d] for d in data]
                    + [self.initial[d] for d in data],
                )

        else:
            with connection.cursor() as cursor:
                cursor.execute(
                    "INSERT INTO pengunjung VALUES(%s, %s, %s, %s)",
                    [self.cleaned_data[d] for d in data],
                )


class OrganizerRegisterForm(UserRegisterForm):
    npwp = forms.CharField(max_length=20, label="NPWP")

    def save(self):
        super().save()

        data = ["email", "npwp"]

        if self.for_update:
            # Used as argument in the SQL query, using the key in data
            set_args = ", ".join([d + "=%s" for d in data])
            where_args = " AND ".join([d + "=%s" for d in data])
            with connection.cursor() as cursor:
                cursor.execute(
                    f"UPDATE organizer SET {set_args} WHERE {where_args}",
                    [self.cleaned_data[d] for d in data]
                    + [self.initial[d] for d in data],
                )

        else:
            with connection.cursor() as cursor:
                cursor.execute(
                    "INSERT INTO organizer VALUES(%s, %s)",
                    [self.cleaned_data[d] for d in data],
                )


class IndividualOrganizerRegisterForm(OrganizerRegisterForm):
    no_ktp = forms.CharField(max_length=20, label="No KTP")
    nama_depan = forms.CharField(max_length=20)
    nama_belakang = forms.CharField(max_length=20)

    def save(self):
        super().save()

        data = ["email", "no_ktp", "nama_depan", "nama_belakang"]

        if self.for_update:
            # Used as argument in the SQL query, using the key in data
            set_args = ", ".join([d + "=%s" for d in data])
            where_args = " AND ".join([d + "=%s" for d in data])
            with connection.cursor() as cursor:
                cursor.execute(
                    f"UPDATE individu SET {set_args} WHERE {where_args}",
                    [self.cleaned_data[d] for d in data]
                    + [self.initial[d] for d in data],
                )

        else:
            with connection.cursor() as cursor:
                cursor.execute(
                    "INSERT INTO individu VALUES(%s, %s, %s, %s)",
                    [self.cleaned_data[d] for d in data],
                )


class CompanyOrganizerRegisterForm(OrganizerRegisterForm):
    nama = forms.CharField(max_length=50)

    def save(self):
        super().save()

        data = ["email", "nama"]

        if self.for_update:
            # Used as argument in the SQL query, using the key in data
            set_args = ", ".join([d + "=%s" for d in data])
            where_args = " AND ".join([d + "=%s" for d in data])
            with connection.cursor() as cursor:
                cursor.execute(
                    f"UPDATE perusahaan SET {set_args} WHERE {where_args}",
                    [self.cleaned_data[d] for d in data]
                    + [self.initial[d] for d in data],
                )

        else:
            with connection.cursor() as cursor:
                cursor.execute(
                    "INSERT INTO perusahaan VALUES(%s, %s)",
                    [self.cleaned_data[d] for d in data],
                )
