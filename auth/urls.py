from django.urls import path

from auth import views


app_name = "auth"

urlpatterns = [
    path("register/", views.register_choose, name="register"),
    path("register/visitor", views.register_visitor, name="register_visitor"),
    path("register/organizer", views.register_organizer, name="register_organizer"),
    path("login/", views.login, name="login"),
    path("logout/", views.logout, name="logout"),
]
