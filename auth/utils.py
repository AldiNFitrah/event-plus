from django.db import connection
from django.contrib.auth.hashers import check_password, make_password


def dictfetchall(cursor):
    "Return all rows from a cursor as a list of dict"
    columns = [col[0] for col in cursor.description]
    return [dict(zip(columns, row)) for row in cursor.fetchall()]


def dictfetchone(cursor):
    "Return one row from a cursor as a dict"
    data = cursor.fetchone()
    if data is None:
        return None

    columns = [col[0] for col in cursor.description]
    return dict(zip(columns, data))


def authenticated(email, password):
    with connection.cursor() as cursor:
        cursor.execute(
            "SELECT * FROM pengguna WHERE email=%s",
            [email]
        )

        # Check whether the email is in db, then validate the password
        data = cursor.fetchone()
        if data is not None and password_valid(password, data[1]):
            return True

    return False


def get_user_role(email):
    table_role = {
        "pengunjung": "visitor",
        "individu": "individual organizer",
        "perusahaan": "company organizer",
    }
    with connection.cursor() as cursor:
        # Check which table stored the corresponding email, determine the role
        for table in table_role:
            cursor.execute(
                f"SELECT * FROM {table} WHERE email=%s LIMIT 1",
                [email]
            )
            if cursor.fetchone() is not None:
                return table_role[table]

    return "user"


def hashed(password):
    "Return only the hashed string, without the hash method string"
    password_hash = make_password(password, "bcrypt_sha256")
    return password_hash.split("$")[-1]


def password_valid(password, password_hash):
    "Concatenate password_hash with the hash method string first"
    return check_password(password, "sha1$bcrypt_sha256$" + password_hash)
