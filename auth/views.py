from django.http import HttpResponse
from django.shortcuts import render, redirect, reverse

from auth.decorators import login_required, login_forbidden
from auth.forms import (
    UserLoginForm,
    VisitorRegisterForm,
    IndividualOrganizerRegisterForm,
    CompanyOrganizerRegisterForm,
)
from auth.utils import authenticated, get_user_role


@login_forbidden()
def register_choose(request):
    return render(request, "register_choose.html")


@login_forbidden()
def register_visitor(request):
    context = {}
    form = VisitorRegisterForm()

    if request.method == "POST":
        form = VisitorRegisterForm(request.POST)

        if form.is_valid():
            try:
                form.save()

                return redirect(
                    reverse("auth:login")
                    + "?message=Registrasi berhasil. Silakan lakukan login."
                )

            # Raised exception from trigger and stored procedure in db
            except Exception as e:
                form.add_error(
                    "email", "Email telah terdaftar. Silakan gunakan email lain"
                )

    context["form"] = form
    return render(request, "register_visitor.html", context)


@login_forbidden()
def register_organizer(request):
    individual_form = IndividualOrganizerRegisterForm()
    company_form = CompanyOrganizerRegisterForm()

    if request.method == "POST":
        # no_ktp only belongs to individual organizer
        if request.POST.get("no_ktp") is not None:
            form = individual_form = IndividualOrganizerRegisterForm(request.POST)

        else:
            form = company_form = CompanyOrganizerRegisterForm(request.POST)

        if form.is_valid():
            try:
                form.save()

                return redirect(
                    reverse("auth:login")
                    + "?message=Registrasi berhasil. Silakan lakukan login."
                )

            # Raised exception from trigger and stored procedure in db
            except Exception as e:
                form.add_error(
                    "email", "Email telah terdaftar. Silakan gunakan email lain"
                )

    context = {
        "individual": {
            "form": individual_form,
            "is_visible": request.POST.get("no_ktp") is not None,
        },
        "company": {
            "form": company_form,
            "is_visible": request.POST.get("no_ktp") is None,
        },
    }
    return render(request, "register_organizer.html", context)


@login_forbidden()
def login(request):
    form = UserLoginForm()
    context = {}

    if request.method == "GET":
        context["message"] = request.GET.get("message", "")

    if request.method == "POST":
        form = UserLoginForm(request.POST)

        if form.is_valid():
            if authenticated(**form.cleaned_data):
                request.session["email"] = form.cleaned_data["email"]
                request.session["role"] = get_user_role(form.cleaned_data["email"])

                next_path = request.GET.get("next", "/")
                return redirect(next_path)

        context["error"] = (
            "Email/Password gagal. Silakan ulangi "
            "kembali dengan informasi yang benar."
        )

    context["form"] = form
    return render(request, "login.html", context)


def logout(request):
    request.session.flush()
    return redirect("/")
