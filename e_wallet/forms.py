from django import forms
from django.db import connection


class EWalletUpdateForm(forms.Form):
    email_pengunjung = forms.EmailField(disabled=True)
    nama = forms.CharField(disabled=True, label="Nama Dompet")
    no_akun = forms.CharField(disabled=True, label="No. Akun")
    saldo = forms.FloatField(disabled=True, label="Saldo")
    top_up_value = forms.IntegerField(min_value=0, max_value=int(1e9), initial=0)
    total_balance = forms.FloatField(min_value=0, initial=0)

    def save(self):
        where_data = ["email_pengunjung", "no_akun"]
        where_args = " AND ".join([d + "=%s" for d in where_data])

        with connection.cursor() as cursor:
            cursor.execute(
                f"UPDATE dompet_digital SET saldo=%s WHERE {where_args}",
                [self.cleaned_data["total_balance"]]
                + [self.initial[d] for d in where_data],
            )
