from django import template
from django.template.defaultfilters import stringfilter


register = template.Library()


@register.filter
def money(value):
    return f"{value:,}".replace(".", ";").replace(",", ".").replace(";", ",")
