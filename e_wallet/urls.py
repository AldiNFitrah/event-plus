from django.urls import path

from e_wallet import views


app_name = "e_wallet"

urlpatterns = [
    path("", views.wallet_list, name="list"),
    path("top-up/<str:account_number>", views.top_up, name="top_up"),
]
