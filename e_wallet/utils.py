from django.db import connection
from django.http import Http404

from auth.utils import dictfetchall, dictfetchone


def get_wallet_list(email):
    with connection.cursor() as cursor:
        cursor.execute(
            "SELECT * FROM dompet_digital WHERE email_pengunjung=%s",
            [email],
        )
        return dictfetchall(cursor)


def get_wallet(email, account_number):
    with connection.cursor() as cursor:
        cursor.execute(
            "SELECT * FROM dompet_digital WHERE email_pengunjung=%s AND no_akun=%s",
            [email, account_number],
        )
        wallet_data = dictfetchone(cursor)

    if wallet_data is None:
        raise Http404()

    return wallet_data
