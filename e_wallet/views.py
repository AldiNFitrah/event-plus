from django.shortcuts import render, redirect

from auth.decorators import login_required, role_required
from e_wallet.forms import EWalletUpdateForm
from e_wallet.utils import get_wallet_list, get_wallet
from user.utils import get_user_data


@login_required()
@role_required("visitor")
def wallet_list(request):
    email = request.session["email"]
    role = request.session["role"]

    context = {
        "user": get_user_data(email, role),
        "wallets": get_wallet_list(email),
    }
    return render(request, "wallet_list.html", context)


@login_required()
@role_required("visitor")
def top_up(request, account_number):
    email = request.session["email"]
    role = request.session["role"]
    user_data = get_user_data(email, role)
    wallet_data = get_wallet(email, account_number)
    wallet = EWalletUpdateForm(initial=wallet_data)

    if request.method == "POST":
        wallet = EWalletUpdateForm(request.POST, initial=wallet_data)

        if wallet.is_valid():
            wallet.save()

            return redirect("e_wallet:list")

    context = {
        "user": user_data,
        "wallet": wallet,
    }
    return render(request, "top_up.html", context)
