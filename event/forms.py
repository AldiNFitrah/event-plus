from django import forms

class EventForm(EventForm):
    nama_event = forms.CharField(max_length=20, label="Nama Event")
    deskripsi_event = forms.CharField(max_length=20, label="Deskripsi Event")
    tema_event = forms.CharField(max_length=20, label="Tema Event")

    def save(self):
        super().save()

        if self.for_update:
            # TODO: Update data in Event table
            pass

        else:
            # TODO: Save data to Event table
            pass
