from django.urls import path

from event import views


app_name = "event"

urlpatterns = [
    path("", views.event_list, name="list"),
    path("create", views.create, name="create"),
    path("<str:id_event>/edit", views.edit, name="edit")
]
