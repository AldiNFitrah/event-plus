from django.db import connection
from django.http import Http404

from auth.utils import dictfetchall, dictfetchone


def get_event_list():
    with connection.cursor() as cursor:
        cursor.execute("""
            SELECT *
            FROM event e
            JOIN organizer o
                ON e.email_organizer=o.email
            LEFT JOIN individu i
                ON o.email=i.email
            LEFT JOIN (
                SELECT email, nama AS nama_perusahaan
                FROM perusahaan
            ) AS p
            ON p.email=o.email;
        """)
        result = dictfetchall(cursor)

        for idx, row in enumerate(result):
            name = (
                row["nama_perusahaan"] if row["nama_perusahaan"] is not None
                else row["nama_depan"] + " " + row["nama_belakang"]
            )
            row["organizer"] = {
                "nama": name,
                "email": row.pop("email_organizer"),
            }
            for unused in ["nama_depan", "nama_belakang", "nama_perusahaan"]:
                row.pop(unused)

            with connection.cursor() as cursor:
                cursor.execute(
                    "SELECT nama_tema FROM tema WHERE id_event=%s",
                    [row["id_event"]],
                )
                theme_list = [v[0] for v in cursor.fetchall()]

                cursor.execute("""
                    SELECT nama_gedung
                    FROM lokasi l
                    JOIN lokasi_event le
                        ON l.kode=le.kode_lokasi
                    WHERE le.id_event=%s
                """,
                    [row["id_event"]],
                )
                location_list = [v[0] for v in cursor.fetchall()]

            row["tema"] = theme_list
            row["lokasi"] = location_list
            row["no"] = idx + 1

        return result
