from django.core.paginator import Paginator
from django.shortcuts import render, redirect

from auth.decorators import login_required, role_required
from event.utils import get_event_list


def event_list(request):
    list_events = get_event_list()
    paginator = Paginator(list_events, 10)

    page_number = request.GET.get("page", 1)
    page_obj = paginator.get_page(page_number)

    context = {
        "page_obj": page_obj,
    }
    return render(request, "event_list.html", context)


@login_required()
@role_required("organizer")
def edit(request, id_event):
    email = request.session["email"]
    role = request.session["role"]
    return render(request, "edit_event.html")


@login_required()
@role_required("organizer")
def create(request):
    email = request.session["email"]
    role = request.session["role"]
    return render(request, "create_event.html")
