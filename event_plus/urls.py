from django.urls import path, include

import event.views


urlpatterns = [
    path("", event.views.event_list),
    path("auth/", include("auth.urls")),
    path("profile/", include("user.urls")),
    path("e-wallet/", include("e_wallet.urls")),
    path("event/", include("event.urls")),
    path("event/<str:id>/tiket/", include("tiket.urls")),
]
