from django import forms


class PesanTicket(forms.Form):
    nama_pemesan = forms.CharField(disabled=True)
    email_pemesan = forms.EmailField(disabled=True)
