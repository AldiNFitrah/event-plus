from django.urls import path

from tiket import views


app_name = "tiket"

urlpatterns = [
    path("buy", views.tiket_list, name="buy"),
    path("transaksi/", views.transactions, name="transaksi"),
    path("transaksi/update", views.update, name="update"),
]
