def get_event_desc():
    return {
        "tema": "IT",
        "nama": "Hackathon",
        "deskripsi": "Hackathon competition",
        "tanggal": "2 Des 2020 - 7 Des 2020",
        "lokasi": "Online",
        "tipe": "Competition",
    }


def get_ticket_list():
    return [
        {
            "nama_kelas": "Gold",
            "tarif": 150000,
            "kapasitas": 200,
            "kapasitas_tersedia": 50,
        },
        {
            "nama_kelas": "Silver",
            "tarif": 100000,
            "kapasitas": 200,
            "kapasitas_tersedia": 75,
        },
        {
            "nama_kelas": "Bronze",
            "tarif": 50000,
            "kapasitas": 200,
            "kapasitas_tersedia": 10,
        },
    ]


def get_transaksi_list():
    return [
        {
            "tanggal": "2020-12-3",
            "waktu": "10:45",
            "status": "Belum dibayar",
            "total_bayar": 10000000,
        },
        {
            "tanggal": "2020-12-4",
            "waktu": "11:00",
            "status": "Lunas",
            "total_bayar": 250000,
        },
    ]
