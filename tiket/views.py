from django.shortcuts import render, redirect

from auth.decorators import login_required, role_required
from tiket.utils import get_ticket_list, get_event_desc, get_transaksi_list


@login_required()
@role_required("visitor")
def tiket_list(request, id_event):
    email = request.session["email"]
    role = request.session["role"]

    context = {
        "desc": get_event_desc(),
        "tickets": get_ticket_list(),
    }

    return render(request, "pesan_tiket.html", context)


@login_required()
@role_required("visitor")
def transactions(request):
    email = request.session["email"]
    role = request.session["role"]

    context = {
        "transactions": get_transaksi_list(),
    }

    return render(request, "transaksi_tiket.html", context)


@login_required()
@role_required("visitor")
def update(request):
    email = request.session["email"]
    role = request.session["role"]

    context = {
        "desc": get_event_desc(),
        "tickets": get_ticket_list(),
    }

    return render(request, "update_transaksi.html", context)
