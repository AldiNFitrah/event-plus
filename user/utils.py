from django.db import connection

from auth.forms import (
    CompanyOrganizerRegisterForm,
    IndividualOrganizerRegisterForm,
    VisitorRegisterForm,
)
from auth.utils import dictfetchone


def get_user_data(email, role):
    user_data = {}

    if "organizer" in role:
        with connection.cursor() as cursor:
            cursor.execute(
                "SELECT npwp FROM organizer WHERE email=%s",
                [email]
            )
            user_data.update(dictfetchone(cursor))

    role_table_mapping = {
        "visitor": "pengunjung",
        "individual organizer": "individu",
        "company organizer": "perusahaan",
    }
    with connection.cursor() as cursor:
        cursor.execute(
            f"SELECT * FROM {role_table_mapping[role]} WHERE email=%s",
            [email]
        )
        user_data.update(dictfetchone(cursor))

    return user_data


def get_update_form(role, data, initial=None):
    if "visitor" in role:
        return VisitorRegisterForm(data, for_update=True, initial=initial)

    elif "individual" in role:
        return IndividualOrganizerRegisterForm(data, for_update=True, initial=initial)

    elif "company" in role:
        return CompanyOrganizerRegisterForm(data, for_update=True, initial=initial)
