from django.shortcuts import render, redirect

from auth.decorators import login_required
from user.utils import get_user_data, get_update_form


@login_required()
def profile(request):
    email = request.session["email"]
    role = request.session["role"]

    user_data = get_user_data(email, role)
    form = get_update_form(role, user_data)

    context = {"form": form}
    return render(request, "profile.html", context)


@login_required()
def edit_profile(request):
    email = request.session["email"]
    role = request.session["role"]

    user_data = get_user_data(email, role)
    form = get_update_form(role, user_data)

    if request.method == "POST":
        form = get_update_form(role, request.POST, user_data)

        if form.is_valid():
            form.save()

            request.session["email"] = form.cleaned_data["email"]
            return redirect("user:profile")

    context = {"form": form}
    return render(request, "edit_profile.html", context)
